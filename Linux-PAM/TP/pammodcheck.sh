#!/bin/bash

#   pammodcheck - display the managers services offered by a PAM module
#   Copyright (C) 2010 Guillaume Allègre <guillaume.allegre@silecs.info>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.


Msg="usage: $0 <pam_foobar.so>"

module=$1

if [[ $# -lt 1 || ! -f $module ]] 
then
	echo $Msg
	exit 1
fi


declare -A pamMgr pamAbbr display

pamMgr=(
	[authenticate]='auth'
	[setcred]='auth'
	[acct_mgmt]='acct'
	[close_session]='sess'
	[open_session]='sess'
	[chauthtok]='pass'
)

pamAbbr=(
	[authenticate]='A'
	[setcred]='C'
	[close_session]='C'
	[open_session]='O'
	[acct_mgmt]='.'
	[chauthtok]='.'
)

list=$( nm -D -p $module \
	| grep 'pam_sm' \
	| sed -e 's/^.*pam_sm_//g' )

for symb in $list
	do 
	manager=${pamMgr[$symb]}
	abbr=${pamAbbr[$symb]}
#	echo $symb $manager $abbr
	display[$manager]+=$abbr
	done

# echo ${display[@]}

echo -n "${#display[@]} $module  "
for key in "${!display[@]}"
do
	echo -n "${key}[${display[$key]}]  "
done
echo ''
