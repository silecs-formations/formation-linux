#!/bin/sh

source ../../pdfl.sh

rev=$(hg sum |grep parent |cut -f2 -d' ')
Rev="Révision $rev"

echo $Rev > def-revision.tex

pdfl 0 hide linux-utilisation-test
pdfl 0 hide linux-lpi-compilation


