\documentclass[A4,10pt]{article} 

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{layout}
% \usepackage{pslatex}
% \usepackage{url}
\usepackage[paper=a4paper,
  left=2cm,top=1.2cm,right=2cm,bottom=1.2cm,
  nohead,foot=0.1cm]{geometry}

\newcommand\FInput[1]{\textit{\bf \LARGE \texttt{#1}}}
\newcommand\cmd[1]{\texttt{#1}}

\begin{document}

% Definition of title page:
\title{Liste des diapos}

\maketitle

\tableofcontents


EXTRACTION


\end{document}
