#!/usr/bin/perl

$Ftpl="./beamerlist.tpl";
$List="enumerate";
$BegL='\begin{'.$List.'}'."\n";
$EndL='\end{'.$List.'}'."\n\n";


$FlagList=0;  # bool : is a List environment open ?
open FTPL, "<", $Ftpl 
	or die "Unable to read $Ftpl.";

while (<FTPL>) {
	if ( /EXTRACTION/) { # here is framelist extraction

		foreach $Ftex (@ARGV) { # for each .tex file
			print "\\FInput{".$Ftex."}\n";
			open FTEX, "<", $Ftex;
			while ($line=<FTEX>) {
				if ($line =~ /frametitle{(.*)}/) {  # frametitle -> item
					if (!$FlagList) { print $BegL; $FlagList=1; }
					print "  \\item $1 \n";
				}
				else { # autre ligne � extraire
					if ($line =~ /\\(sub)?section(\[.*\])?\{(.*)\}/) {
							if ($FlagList) { print $EndL; $FlagList=0; }
							print "\\".$1."section{".$2."  ".$3."} \n";
							}
					}
				}			

			if ($FlagList) { print $EndL; $FlagList=0; }
			close FTEX;
		}
		
	}
	else { # normal template line
		print $_;
	}

}

close FTPL;
