#-- Trouver les articles de plus de 50 euros.

#-- Lister les noms des articles, triés par prix.
# Les trier par catégorie, puis par stock pour une même catégorie.

#-- Afficher toutes les commandes de 2004, puis les 3 commandes les plus récentes.
# OU   

#-- Quelles différences entre les deux lignes : 
# SELECT COUNT(*) FROM articles ;
# SELECT COUNT(articles.codearticle) FROM articles ;

#-- Combien d'articles de squash a-t-on ?



# Afficher tous les articles classés dans la même catégorie que le tuba

# Afficher tous les articles moins chers que le tuba
# jointure

# Variante sous-requête 


# Trouver tous les prix qui ont changé entre la commande (detail.prix)
# et le catalogue (article.prix)

# Méthode 1 : jointure

# Méthode 2 : UNION + sous-requête 

# Méthode 3 (mauvaise) : focus uniquement sur les prix



# Trouver l''article le plus cher (code, nom, prix)
# Par le plus grand nombre de méthodes différentes !

# Méthode 0 : inconvénient ?

# Méthode 1 : sous-requête

# Méthode 2 : variable intermédiaire

# Méthode 3 : auto-jointure

# Méthode 4 : sous-requête sans agrégation



# 1. Trouver tous les prix multiples (prix identiques pour plusieurs articles)
# 2. Afficher tous les articles concernés
# Q1 :

# 2.a  méthode 1 : auto-jointure

# 2.b méthode 2 : sous-requêtes

# 2.c méthode 3 : presque de la triche !



# Assigner un rang aux articles, classés par prix décroissant
# (numéro 1 = le plus cher).
# La requête affiche une table articles "augmentée" sans modifier l''originale

# 1. rang "naïf" : on ne tient pas compte des égalités

# 2. rang "statistique" : on tient compte des égalités, on ne saute pas de rang

# 3. rang "sportif" : on tient compte des égalités, on saute les rangs attribués aux ex-aequo


# Q1. Afficher un total des commandes par année :
# Afficher le montant de chaque commande, avec un sous-total par année :

# Afficher une vue "anonymisée" des clients : Prénom, initiale du nom, département 
# Même question mais sous la forme d''une seule colonne

# Afficher toutes les commandes qui ont eu lieu il y a moins d''un an, et leur délai en jours



# Ajouter à la vue précédente la date de facturation : 1er du mois suivant la commande

# Variante : le dernier jour du mois de la commande :


# Extraire la partie d''une chaîne située avant ":" (deux méthodes) :
