
CREATE TABLE `Disques` (
  `id_disque` mediumint(8) unsigned NOT NULL auto_increment,
  `titre` varchar(50) NOT NULL,
  `date` DATE NOT NULL,
  `commentaire` text NOT NULL,
  `id_label` mediumint(9) unsigned default NULL,
  PRIMARY KEY  (`id_disque`)
) ;

CREATE TABLE `Labels` (
  `id_label` mediumint(8) unsigned NOT NULL auto_increment,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_label`)
) ;

CREATE TABLE `Genres` (
  `id_genre` mediumint(8) unsigned NOT NULL auto_increment,
  `intitule` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_genre`)
) ;

CREATE TABLE `Disques_Genres` (
  `id_disque` mediumint(8) unsigned NOT NULL,
  `id_genre` mediumint(8) unsigned NOT NULL
) ;

CREATE TABLE `Artistes` (
  `id_artiste` mediumint(8) unsigned NOT NULL auto_increment,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_artiste`)
) ;

CREATE TABLE `Disques_Artistes` (
  `id_disque` mediumint(8) unsigned NOT NULL,
  `id_artiste` mediumint(8) unsigned NOT NULL,
  `typeRelation` ENUM('interprete', 'compositeur') NOT NULL
) ;

