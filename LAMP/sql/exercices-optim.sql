# exemples pour les EXPLAIN :
# colonnes id et select_type
#
SET global query_cache_size:=0;
explain SELECT codearticle FROM articles WHERE prix >50.0 ;
explain SELECT codearticle FROM articles WHERE prix = (SELECT MAX(prix) FROM articles) ;
explain SELECT c.nom, a.*  FROM articles a JOIN categories c USING (idcategorie) ;
explain SELECT prix FROM articles UNION SELECT prix from details; 
explain SELECT MAX(prix) FROM (SELECT prix from articles) tssreq ; 
#
explain SELECT * FROM articles WHERE codearticle IN (SELECT codearticle FROM details ) ;
explain SELECT * FROM articles WHERE codearticle IN (SELECT codearticle FROM details WHERE details.codearticle=articles.codearticle) ;
#
# colonne table : ordre des lignes EXPLAIN
#
explain SELECT *  FROM articles a   JOIN categories c USING (idcategorie) ;
explain SELECT *  FROM categories c JOIN articles a   USING (idcategorie) ;
#
# colonne type : type accès aux enregistrements
#
explain SELECT * FROM communes ;
explain SELECT depcom FROM communes ;
explain SELECT depcom, naiss FROM  naissances WHERE depcom='38185';
explain SELECT depcom, naiss FROM communes JOIN naissances USING (depcom) WHERE depcom='38185';
explain SELECT depcom, naiss FROM communes JOIN naissances USING (depcom) WHERE communes.depcom='38185';
explain SELECT depcom, naiss FROM communes JOIN naissances USING (depcom) WHERE naissances.depcom='38185';

explain SELECT * FROM communes where depcom LIKE '38%';
explain SELECT * FROM communes where depcom='38185';
explain SELECT * FROM communes where depcom IN (38169, 38170, 38171);
explain SELECT MAX(depcom) FROM communes; 

explain SELECT depcom, naiss FROM communes JOIN naissances USING (depcom) WHERE depcom=38185;
# avec et sans un index sur la colonne annee :
explain SELECT MAX(naiss) FROM naissances WHERE annee = 2000 ;
explain SELECT MAX(naiss) FROM naissances WHERE annee = 2000 AND depcom LIKE '38%' ;
explain SELECT MAX(naiss) FROM naissances WHERE depcom LIKE '38%' ;

explain SELECT COUNT(*) FROM communes c JOIN naissances n1 USING (depcom) 
JOIN naissances n2 ON (n2.depcom=n1.depcom AND n1.naiss = n2.naiss AND n1.annee=2007 AND n2.annee=1999) ;

## Comment optimiser la requête suivante ?
# SELECT c.libmin, c.psdc99 FROM communes c WHERE psdc99 IN (
# SELECT MAX(psdc99) FROM communes c GROUP BY dep
#  ) ;
## 
SELECT c.dep, c.libmin, c.psdc99 FROM communes c JOIN 
    (SELECT dep, MAX(psdc99) AS pop FROM communes c GROUP BY dep) AS MaxDep 
    ON c.psdc99=MaxDep.pop ;


# étudier les explain des requêtes suivantes :

SELECT *  
FROM naissances n 
LEFT JOIN communes c USING (depcom) 
WHERE c.depcom IS NULL ;

SELECT n.*       
FROM naissances n
LEFT JOIN communes c USING (depcom)
WHERE c.depcom IS NULL ;
 
#-- utilité d'un index sur la colonne depcom de la table naissances ?

SELECT depcom 
FROM naissances n 
LEFT JOIN communes c USING (depcom) 
WHERE c.depcom IS NULL ;

SELECT DISTINCT depcom 
FROM naissances n 
LEFT JOIN communes c USING (depcom) 
WHERE c.depcom IS NULL ;

SELECT depcom, count(*) 
FROM naissances n 
LEFT JOIN communes c USING (depcom) 
WHERE c.depcom IS NULL  GROUP BY depcom;




# Méthode 0 : inconvénient ?
SELECT libmin, depcom, psdc99 
  FROM naiss2007 
ORDER BY psdc99 DESC LIMIT 1;

# Méthode 1 : sous-requête
SELECT libmin, depcom, psdc99 
  FROM naiss2007 
 WHERE psdc99 = (SELECT MAX(psdc99) FROM naiss2007);

# Méthode 2 : variable intermédiaire
SET @maxi=(SELECT MAX(psdc99) FROM naiss2007) ;
SELECT libmin, depcom, psdc99 FROM naiss2007 WHERE psdc99=@maxi ;

# Méthode 3 : auto-jointure
SELECT n1.depcom, n1.libmin, n1.psdc99  
  FROM naiss2007 n1 
LEFT JOIN naiss2007 n2 ON (n1.psdc99 < n2.psdc99) 
WHERE n2.depcom IS NULL ;

# Méthode 4 : sous-requête sans agrégation
SELECT libmin, depcom, psdc99 
  FROM naiss2007 
WHERE psdc99 >= ALL(SELECT psdc99 FROM naiss2007) ;

