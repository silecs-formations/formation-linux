# Exercices sur les vues 
# Définir une vue Vcommandes, qui augmente la table commande 
# avec le nom du client, le nb de lignes, les champs quantite et montant

# Définir une vue Vfactures, qui donne les mêmes informations, plus les détails :
# une ligne par détail, plus une ligne de totalisation 
# astuce : GROUP BY WITH ROLLUP 




# Définir une fonction qui prend une chaîne et retourne
# cette chaîne avec la première lettre en capitale, 
# et les suivantes en minuscules
# Cf : left(), mid(), right(), upper(), lower(), concat()



# Définir une fonction PreNom qui prend deux chaînes (prénom, nom)
# et sort les deux concaténés et bien stylés (initiales en majuscule)




# Dans facsys, une fonction qui retourne le montant total commandé
# par un client



#  À partir du nom d'un nouveau client, retourner un nouveau idclient unique 
# (rappel : 3 premiers caractères du nom, suivis d'un numéro, par ex. DUR005).




# Pour poursuivre le montant total : une procédure qui retourne en plus
# le nombre total d''articles commandés



# Procédure Pfacture : retourne d'abord l'en-tête (Client, adresse)
# puis la liste des détails, puis le total, pour une commande donnée.



# Définir une routine qui affiche le mode (valeur la plus fréquente)
# de la colonne prix de la table articles





# Définir une routine qui affiche la médiane d''une liste de valeurs
# Méthode 1 : PREPARE et LIMIT 


# Définir une routine qui affiche la médiane d''une liste de valeurs
# Méthode 2 : fonction et rang



# Définir une routine qui affiche la médiane d''une liste de valeurs
# Méthode 3 : fonction et curseur





# Définir une routine qui affiche la somme des montants des N articles les plus chers
# et la somme totale du stock correspondant
#




# Implémenter une fonction maximum avec plafond sur les prix des articles :
# ne prend pas en compte les valeurs supérieures au plafond
# avec un curseur et un handler ; 
# 





# TRIGGERS 
# Mettre à jour le stock quand une commande arrive
#

# Ajouter un compteur des commandes et des montants du jour



# Si on commande des balles de squash : 5 minimum !


