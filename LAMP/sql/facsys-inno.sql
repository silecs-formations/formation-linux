-- MySQL dump 10.11
--
-- Host: localhost    Database: facsys
-- ------------------------------------------------------
-- Server version	5.0.51a-3-log

--
-- Table structure for table `articles`
--
CREATE DATABASE IF NOT EXISTS `facsys_inno` ;
USE `facsys_inno` ;

DROP TABLE IF EXISTS `articles`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `articles` (
  `codearticle` char(5) NOT NULL default '',
  `nom` char(50) default NULL,
  `prix` float(6,2) default NULL,
  `stock` smallint(5) unsigned default NULL,
  `idcategorie` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`codearticle`),
  KEY `idcategorie` (`idcategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES ('SHR03','Short court',17.50,2,3);
INSERT INTO `articles` VALUES ('BOU89','Bouchon rond',43.00,2,1);
INSERT INTO `articles` VALUES ('CAP01','canne a peche',80.40,4,1);
INSERT INTO `articles` VALUES ('RAQ01','raquette de squash',62.00,3,2);
INSERT INTO `articles` VALUES ('BAL45','Balle de squash debutant',5.00,35,2);
INSERT INTO `articles` VALUES ('BAL55','balle de squash joueur averti',9.00,30,2);
INSERT INTO `articles` VALUES ('SHR04','short femme',21.50,5,4);
INSERT INTO `articles` VALUES ('SHR05','short homme',20.90,8,5);
INSERT INTO `articles` VALUES ('MAI11','maillot femme une piece',26.00,4,6);
INSERT INTO `articles` VALUES ('MAI12','maillot femme deux pieces',24.00,10,6);
INSERT INTO `articles` VALUES ('MAI21','calecon de bain',17.50,15,6);
INSERT INTO `articles` VALUES ('PAL04','palmes loisir',32.00,3,7);
INSERT INTO `articles` VALUES ('TUB01','tuba',6.50,12,7);
INSERT INTO `articles` VALUES ('BOU03','bouteilles air',89.90,2,7);
INSERT INTO `articles` VALUES ('HAR17','fusil-harpon',199.00,1,7);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `categories` (
  `idcategorie` tinyint(3) unsigned NOT NULL auto_increment,
  `nom` varchar(30) default NULL,
  `description` varchar(50) default NULL,
  PRIMARY KEY  (`idcategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'peche','les articles de peche');
INSERT INTO `categories` VALUES (2,'Squash','articles de squash');
INSERT INTO `categories` VALUES (3,'Habillement enfant','du 0 au 16 ans');
INSERT INTO `categories` VALUES (4,'Habillement femme','femme, a partir de 16 ans');
INSERT INTO `categories` VALUES (5,'Habillement homme','homme, a partir de 16 ans');
INSERT INTO `categories` VALUES (6,'Natation','Maillots, bonnets, lunettes, etc');
INSERT INTO `categories` VALUES (7,'Plongee','Plongee sous-marine');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `clients` (
  `idclient` char(6) NOT NULL default '',
  `nom` char(30) default NULL,
  `prenom` char(30) default NULL,
  `adresse` char(50) default NULL,
  `codepostal` int(5) default NULL,
  `ville` char(30) default NULL,
  `telephone` int(10) default NULL,
  PRIMARY KEY  (`idclient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES ('DUR001','Durand','Pierre','Rue du menhir',44500,'Nantes',240955689);
INSERT INTO `clients` VALUES ('BLI034','Blineau','Daniel','La motte',85260,'Herbergement',251429803);
INSERT INTO `clients` VALUES ('TES023','Tesson','Alain','1 av de la mer',56546,'Saint Florent',NULL);
INSERT INTO `clients` VALUES ('DUR004','Durand','Sylvain','Place Mayeu',75000,'Paris',109457698);
INSERT INTO `clients` VALUES ('LEC002','Lechat','Maia','12 rue Madeleine',38000,'Grenoble',401234567);
INSERT INTO `clients` VALUES ('CHA001','Chacat','Thierry','8 rue de l\'Estaque',13006,'Marseille',419123456);
INSERT INTO `clients` VALUES ('PLI004','Plisson','Daniel','5 montée du Chien qui fume',84860,'Caderousse',498765432);
INSERT INTO `clients` VALUES ('LEC019','Lechien','Gerard','4 Bd du temps',34270,'Sete',487652347);
INSERT INTO `clients` VALUES ('DOI009','Doisnel','Antoine','134 Bd des Champs Elysees',75001,'Paris',145127856);
INSERT INTO `clients` VALUES ('LOI014','Loiseau','Colette','12 quai des Brumes',75007,'Paris',NULL);
INSERT INTO `clients` VALUES ('LEF001','Lefranc','Martin','5 allée de la Truffe noire',30000,'Nimes',NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `commandes` (
  `numcommande` int(10) unsigned NOT NULL auto_increment,
  `idclient` char(6) default NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`numcommande`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `commandes`
--

LOCK TABLES `commandes` WRITE;
/*!40000 ALTER TABLE `commandes` DISABLE KEYS */;
INSERT INTO `commandes` VALUES (1,'DUR001','2003-01-19 14:41:35');
INSERT INTO `commandes` VALUES (2,'BLI034','2003-07-24 09:05:14');
INSERT INTO `commandes` VALUES (3,'TES023','2004-07-13 21:56:00');
INSERT INTO `commandes` VALUES (4,'DUR001','2004-12-18 15:45:00');
INSERT INTO `commandes` VALUES (5,'LEC002','2006-09-18 07:53:27');
INSERT INTO `commandes` VALUES (6,'PLI004','2007-09-10 15:56:23');
INSERT INTO `commandes` VALUES (7,'DOI009','2008-04-20 10:55:10');
INSERT INTO `commandes` VALUES (8,'LOI014','2008-06-14 09:17:55');
INSERT INTO `commandes` VALUES (9,'LEF001','2008-09-09 06:23:12');
/*!40000 ALTER TABLE `commandes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `details` (
  `numcommande` int(10) unsigned NOT NULL default '0',
  `numordre` tinyint(3) unsigned NOT NULL default '0',
  `codearticle` char(5) default NULL,
  `quantite` smallint(5) unsigned default NULL,
  `prix` float(7,2) NOT NULL default '0.00',
  PRIMARY KEY  (`numcommande`,`numordre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `details`
--

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` VALUES (1,1,'CAP01',2,80.40);
INSERT INTO `details` VALUES (1,2,'BAL45',3,5.00);
INSERT INTO `details` VALUES (1,3,'RAQ01',1,62.00);
INSERT INTO `details` VALUES (2,1,'SHR03',1,17.50);
INSERT INTO `details` VALUES (3,1,'BAL55',5,9.00);
INSERT INTO `details` VALUES (4,1,'TUB01',2,6.50);
INSERT INTO `details` VALUES (5,1,'RAQ01',1,62.00);
INSERT INTO `details` VALUES (5,2,'BAL45',3,5.00);
INSERT INTO `details` VALUES (6,1,'CAP01',1,80.40);
INSERT INTO `details` VALUES (7,1,'BOU03',2,89.90);
INSERT INTO `details` VALUES (8,1,'MAI11',1,26.00);
INSERT INTO `details` VALUES (8,2,'MAI12',1,24.00);
INSERT INTO `details` VALUES (8,3,'MAI21',2,13.50);
INSERT INTO `details` VALUES (9,1,'HAR17',1,199.00);
/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;

-- Dump completed on 2008-09-28 10:03:06
