-- 
-- Base de données: 'tennis'
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table 'Joueurs'
-- 

DROP TABLE IF EXISTS Joueurs;
CREATE TABLE Joueurs (
  nom varchar(50) NOT NULL,
  id_pays tinyint(4) default NULL,
  PRIMARY KEY  (nom)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Joueurs'
-- 

INSERT INTO Joueurs (nom, id_pays) VALUES ('Federer', 1);
INSERT INTO Joueurs (nom, id_pays) VALUES ('Nadal', 2);
INSERT INTO Joueurs (nom, id_pays) VALUES ('Ferrer', 2);

-- --------------------------------------------------------

-- 
-- Structure de la table 'Pays'
-- 

DROP TABLE IF EXISTS Pays;
CREATE TABLE Pays (
  id_pays tinyint(4) NOT NULL,
  pays varchar(50) NOT NULL,
  PRIMARY KEY  (id_pays)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Pays'
-- 

INSERT INTO Pays (id_pays, pays) VALUES (1, 'Suisse');
INSERT INTO Pays (id_pays, pays) VALUES (2, 'Espagne');
INSERT INTO Pays (id_pays, pays) VALUES (3, 'France');
