-- 
-- Base de données: 'tests_CD'
-- 

SET NAMES 'utf8' ;

-- --------------------------------------------------------

-- 
-- Structure de la table 'Artistes'
-- 

DROP TABLE IF EXISTS Artistes;
CREATE TABLE Artistes (
  id_artiste mediumint(8) unsigned NOT NULL auto_increment,
  nom varchar(50) NOT NULL,
  prenom varchar(50) NOT NULL,
  PRIMARY KEY  (id_artiste)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Artistes'
-- 

INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (1, 'Bach', 'Jean-Sébastien');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (2, 'Beethoven', 'Ludwig');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (3, 'Tchaikovsky', 'Piotr');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (4, 'Mozart', 'Wolfang');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (5, 'Johnny', '');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (6, 'Gould', 'Glenn');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (7, 'Leonhardt', 'Gustav');
INSERT INTO Artistes (id_artiste, nom, prenom) VALUES (8, 'Barbara', '');

-- --------------------------------------------------------

-- 
-- Structure de la table 'Disques'
-- 

DROP TABLE IF EXISTS Disques;
CREATE TABLE Disques (
  id_disque mediumint(8) unsigned NOT NULL auto_increment,
  titre varchar(50) NOT NULL,
  `date` date NOT NULL,
  commentaire text NOT NULL,
  id_label mediumint(9) unsigned default NULL,
  PRIMARY KEY  (id_disque)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Disques'
-- 

INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (1, 'Variations Goldberg', '1953-12-10', 'Premier enregistrement', 1);
INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (2, 'Souvenir de Florence', '2007-01-22', 'Romantique', 2);
INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (3, 'Cantate BWV 10', '2006-10-10', 'Voix d''enfants', 3);
INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (4, 'Variations Diabelli', '2006-03-28', '', 2);
INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (5, 'Les noces de Figaro', '2007-04-27', '', 1);
INSERT INTO Disques (id_disque, titre, date, commentaire, id_label) VALUES (6, 'Allumer le feu', '2006-05-20', '', 4);

-- --------------------------------------------------------

-- 
-- Structure de la table 'Disques_Artistes'
-- 

DROP TABLE IF EXISTS Disques_Artistes;
CREATE TABLE Disques_Artistes (
  id_disque mediumint(8) unsigned NOT NULL,
  id_artiste mediumint(8) unsigned NOT NULL,
  typeRelation enum('interprete','compositeur') NOT NULL
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Disques_Artistes'
-- 

INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (1, 1, 'compositeur');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (2, 3, 'compositeur');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (3, 1, 'compositeur');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (4, 2, 'compositeur');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (5, 4, 'compositeur');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (6, 5, 'interprete');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (1, 6, 'interprete');
INSERT INTO Disques_Artistes (id_disque, id_artiste, typeRelation) VALUES (3, 7, 'interprete');

-- --------------------------------------------------------

-- 
-- Structure de la table 'Disques_Genres'
-- 

DROP TABLE IF EXISTS Disques_Genres;
CREATE TABLE Disques_Genres (
  id_disque mediumint(8) unsigned NOT NULL,
  id_genre mediumint(8) unsigned NOT NULL
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Disques_Genres'
-- 

INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (1, 1);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (1, 4);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (2, 2);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (3, 1);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (3, 5);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (4, 2);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (5, 6);
INSERT INTO Disques_Genres (id_disque, id_genre) VALUES (6, 3);

-- --------------------------------------------------------

-- 
-- Structure de la table 'Genres'
-- 

DROP TABLE IF EXISTS Genres;
CREATE TABLE Genres (
  id_genre mediumint(8) unsigned NOT NULL auto_increment,
  intitule varchar(100) NOT NULL,
  PRIMARY KEY  (id_genre)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Genres'
-- 

INSERT INTO Genres (id_genre, intitule) VALUES (1, 'Baroque');
INSERT INTO Genres (id_genre, intitule) VALUES (2, 'Romantique');
INSERT INTO Genres (id_genre, intitule) VALUES (3, 'Chanson française');
INSERT INTO Genres (id_genre, intitule) VALUES (4, 'Instrument seul');
INSERT INTO Genres (id_genre, intitule) VALUES (5, 'Musique vocale');
INSERT INTO Genres (id_genre, intitule) VALUES (6, 'Opera');

-- --------------------------------------------------------

-- 
-- Structure de la table 'Labels'
-- 

DROP TABLE IF EXISTS Labels;
CREATE TABLE Labels (
  id_label mediumint(8) unsigned NOT NULL auto_increment,
  nom varchar(50) NOT NULL,
  PRIMARY KEY  (id_label)
) ENGINE=MyISAM ;

-- 
-- Contenu de la table 'Labels'
-- 

INSERT INTO Labels (id_label, nom) VALUES (1, 'Phillips');
INSERT INTO Labels (id_label, nom) VALUES (2, 'Harmonia Mundi');
INSERT INTO Labels (id_label, nom) VALUES (3, 'EMI');
INSERT INTO Labels (id_label, nom) VALUES (4, 'Vivendi Universal');
