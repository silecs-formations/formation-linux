#!/bin/sh
# ireadlink : iterative readlink


#set -x
#set -t

if [[ $# -lt 1 ]] 
then 
	echo "iterative readlink :"
	echo "usage : $0 <link>"
	exit
fi

TARGET=$1
N=0
while [ -L $TARGET ] ; 
do 
	echo -n "$TARGET -> " 
	TARGET=$( readlink $TARGET )
	N=$(( N + 1 ))
done
echo $TARGET
echo "depth=$N"

