## Avec la commande find :
##
## 1. Trouver le nb d'entrées de répertoire de chaque type sous /,
##    sans changer de système de fichiers (option -xdev).

for T in f d l b c p s 
do
    echo -ne "type $T : " ; 
    find / -xdev -type $T |wc -l ;
done

# résultat sur hulotte : 
# type f=273427  d=23483  l=16853   s=21  p=3  c=1  b=0

##
## 2. Pour les quatre types minoritaires, afficher les entrées.
## 2 solutions : 
## 2.a en adaptant la boucle ci-dessus avec : for T in b c p s
## 2.b en utilisant la syntaxe de find, avec des ou logiques :
find / -xdev \( -type b -or -type c -or -type b -or -type p -or -type s \) -printf "%Y %p \n"

