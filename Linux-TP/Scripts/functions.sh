#! /bin/sh

function proclevel {
# affiche le niveau de profondeur du processus passé en paramètre
# si pas de paramètre ($1), le PID du shell courant ($$)
local LEV=0 LIST='';
declare -i MPID
MPID=${1:-$$}

while ( ps -oppid -p $MPID &> /dev/null) ; 
do 
	LEV=$(( $LEV+1 )) ; 
	TXT=$( ps -p $MPID -ocomm --no-headers )
	LIST="$MPID=$TXT  $LIST"
	MPID=`ps -oppid -p$MPID |tail -1` ; 
done ; 
echo "$LEV ( $LIST ) "
}


function ireadlink {
# affiche une résolution itérative des liens symboliques
FILE=$1;
echo -n $FILE ;
while (readlink $FILE >/dev/null) ; 
do
	FILE=$(readlink $FILE)
	echo -n " -> $FILE"
done
echo ""
}

# compte les types de fichiers présents sous le répertoire passé en argument 
function typecount() {
	if [[ $# -lt 1 ]] 
	then 
		echo "typecount <DIR> : 1 argument nécessaire"
		return 1
	fi
	find $1 -type f -exec file -i {} \;  | \
		cut -f 2 -d ':'  | \
		sort | \
		uniq -c
}

function typedu() {
	if [[ $# -lt 1 ]] 
	then 
		echo "typedu <DIR> : 1 argument nécessaire"
		return 1
	fi

	declare -A ftype
	while read A B C 
		do  
		# echo $A ${B%;} # débogage
		((ftype[${B%;}]+=$A))  
		done < \
	<( find . -type f -printf "%s " -exec file -i {} \;  | sed -r -e 's/\S+://' ) 
	echo "Totaux :"
	for T in ${!ftype[@]}
		do 
		echo $T : ${ftype[$T]} 
		done
}

