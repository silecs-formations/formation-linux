#!/bin/sh
# rmbrlink : remove a file only if it's a broken symbolic link

if [ -L $1 ]
then 
    readlink -e $1
	if [ $? -eq 1 ]
		then
		unlink $1
		echo "Broken link: deleted"
	else
		echo "Active link: saved"
	fi 
else 
    echo "This file ($1) isn't a symbolic link"
fi

