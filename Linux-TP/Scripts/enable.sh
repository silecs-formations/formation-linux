#! /bin/sh

# (suite de disable.sh)
# Ecrire le script inverse, enable.sh, qui supprime le suffixe .OFF 
# Il doit accepter en argument les deux variantes 
# fichier  OU  fichier.OFF

if [ -e $1 ] ;

then  # l'argument existe donc il est sous la forme fichier.OFF
	FICH=$(basename $1 .OFF) ;
	mv $1 $FICH ;

else # argument fichier
	mv $1.OFF $1 ;

fi

