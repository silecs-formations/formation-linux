#!/bin/sh
# trouve le point de montage auquel se rattache un chemin 
# du système de fichiers

function getmountpoint() {
FILE=$1
LMIN=999
MNT=''   # le point de montage à déterminer

MOUNTPOINTS=`mount -t ext3 |grep -o "on /.* type" |grep -o "/[^ ]*" `

for MNTPT in $MOUNTPOINTS ;
	do
	if [ $MNTPT != '/' ] ;
	then
		MNTPT=${MNTPT}/ ;
	fi
	SUBST=${FILE##$MNTPT}
	L=${#SUBST}

	if [ $L -lt $LMIN ] ;
	then
		LMIN=$L;
		MNT=$MNTPT;
	fi;
	done

echo $MNT
return 0
}
