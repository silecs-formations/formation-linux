#! /bin/sh

#  Transformer les deux scripts en un seul xable.sh, 
#  qui prend une option (-d ou -e) 
#  pour indiquer le sens de l'opération.

OPT=$1
FILE=$2

if [ "$OPT" = "-d" ] ;

then 
	./disable.sh $FILE;
	exit 0;

elif [ "$OPT" = "-e" ] ;
then 
	./enable.sh $FILE;
	exit 0;

else 
	echo "Option inconnue : -d (disable) ou -e (enable)."
	exit 1;
fi
