Un vieillard en or avec une montre en deuil
Et des travailleurs de la paix avec des gardiens de la mer
Un hussard de la farce avec un dindon de la mort
Un serpent à café avec un moulin à lunettes
Un chasseur de corde avec un danseur de têtes
Un compositeur de potence avec un gibier de musique
Une petite soeur du Bengale avec un tigre de Saint-Vincent-de-Paul

Prévert, Cortège (extrait)

