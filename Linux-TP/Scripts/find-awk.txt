##
## Exo 1.
## Trouver la place occupée par l'ensemble des fichiers de plus de 1 Mo
## dans le répertoire utilisateur
## (on peut varier les critères...)
##
find /home/allegre/Perso -size +1M -printf "%s %p \n" | \
    awk '{tot+=$1} END {print tot} '

##
## Exo 2.
## Trouver la place occupée par les fichiers de chaque type (txt, sh, ...)
## dans le répertoire utilisateur
# find /home/allegre/Perso -size +1M -printf "%s %f \n" \
#     | tr . " "  \
#     | awk '{print $1,$3 ; total[$3]+=$1} 
#            END {for (ext in total) {print ext, total[ext] }  } '
	   
find /home/allegre -size +1M -printf "%s %f \n" \
    | awk '{ split($2, array, ".") ;
    	     suff=array[2] ;
	     if (suff == "") {suff="NULL" ;}
    	     totsize[suff]+=$1; totnb[suff]++ } 
           END {for (ext in totsize) {print ext, totsize[ext], totnb[ext] } } '
