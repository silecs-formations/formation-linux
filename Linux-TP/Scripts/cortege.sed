# Basic RE
# s/\(.*\) \(en .*\) avec \(.*\) \(en .*\)/\1 \4 avec \3 \2/
# s/\(.*\) \(de .*\) avec \(.*\) \(de .*\)/\1 \4 avec \3 \2/
s/\(.*\) \(\(en\|de\|du\|à\) .*\) avec \(.*\) \(\(en\|de\|du\|à\) .*\)/\1 \5 avec \4 \2/


# Extended RE
# s/(.*) (en .*) avec (.*) (en .*)/\1 \4 avec \3 \2/
# s/(.*) ((en|de|du|à) .*) avec (.*) ((en|de|du|à) .*)/\1 \5 avec \4 \2/
