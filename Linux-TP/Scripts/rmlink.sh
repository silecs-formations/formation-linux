#!/bin/sh
# rmlink : remove a file only if it's a symbolic link

if [ -L $1 ]
then 
    unlink $1
else 
    echo "This file ($1) isn't a symbolic link"
fi

