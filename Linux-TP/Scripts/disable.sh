#! /bin/sh

# Ecrire un script disable.sh qui 
#   prend en argument un nom fichier
#   le renomme en lui ajoutant le suffixe .OFF 
#   ex. : disable.sh monfichier   ->  renomme monfichier en monfichier.OFF

mv $1 $1.OFF

