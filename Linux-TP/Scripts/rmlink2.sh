#! /bin/bash
# rmlink2 : remove a file only if it's a symbolic link
#          -b : and only if it's broken

if [ $# -lt 1 ] ; 
then 
	echo 'usage: rmlink2 [-b] <direntry>'
	echo 'delete direntry if it's a symbolic link'
	echo '	-b : and only if it's broken.'
	exit 1
fi

if [ $1 = '-b' ] ;
then
	BrokenOnly="V"
	FILE=$2
else
	FILE=$1
fi

if [ -L $FILE ] ;
then
	if [ -n "$BrokenOnly" ] ;
	then 
	    readlink -e $FILE > /dev/null
		if [ $? -eq 1 ] ;
			then
			unlink $FILE
			echo 'Broken link: deleted'
		else
			echo 'Active link: saved'
		fi ;
	else
		unlink $FILE
		echo "$FILE: link deleted."
	fi ;
else 
    echo "$FILE: this file is not a symbolic link."
	exit 1
fi

