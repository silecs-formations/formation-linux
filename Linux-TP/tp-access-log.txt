0. Combien de hits sont enregistrés dans le fichier access.log ?

1. Extraire du fichier access.log la liste des adresses IP clientes.

2. Compter :
  a. le nombre d'occurrences de chaque IP.
  b. le nombre d'adresses IP différentes

3. Présenter la liste par nombre décroissant d'occurrences.

4. Afficher uniquement les IP ayant effectué au moins 10 (resp. 5) accès.

5. Question subsidiaire : pour chacune des IP de la liste précédente, 
   effectuer une résolution de nom (commande host).
 a. en passant par un fichier temporaire
 b. sans intermédiaire, en une seule ligne de commande
 c. comment obtenir à la fois la résolution et le nb d'occurrences ?
