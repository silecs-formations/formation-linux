#!/usr/bin/perl -w
use Cwd qw/realpath/;
open MTAB, "/etc/mtab";
@mounts = <MTAB>;
$matchsize = 0;
foreach (@mounts) {
 @m = split /\s+/;
 $pathsize = length($m[1]);
 if ( (substr(realpath($ARGV[0]),0,$pathsize) eq $m[1])
  && ($pathsize > $matchsize) ) {
  $matchsize = $pathsize;
  $mpoint = $m[1];
 }
}
print "$mpoint\n";
