#!/bin/bash

# TP03  Exercice 1

if [ ! -e $1 ]
then
  echo "Le fichier $1 n'existe pas"
  exit 1
fi

# Info sur le type de fichier
echo -n "Le fichier $1 est un "
if [ -f $1 ]
then
  echo "fichier ordinaire"
elif [ -d $1 ]
then
  echo "repertoire"
else
  echo "truc bizarre"
fi

# Infos sur les droits d'acces
echo -n "qui est accessible par `id -un` en "
if [ -r $1 ]
then
  echo -n " lecture"
fi
if [ -x $1 ]
then
  echo -n " execution"
fi
if [ -w $1 ]
then
  echo -n " ecriture"
fi

echo
