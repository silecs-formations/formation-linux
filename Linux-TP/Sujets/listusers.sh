#!/bin/bash

# TP 3, Exercice 3

# Liste les utilisateurs ayant un UID > 500

# Verification du nombre d'arguments
if [ $# -ne 0 ]
then
   echo "Erreur: usage: $0"
fi

for info in $(cut -d: -f 1,3 /etc/passwd)
do
   uid=$(echo $info | cut -d: -f 2)
   if [ $uid -ge 500 ]
   then
       echo $info | cut -d: -f 1
   fi
done



