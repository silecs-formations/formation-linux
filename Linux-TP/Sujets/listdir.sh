#!/bin/bash

# TP03  Exercice 2

# Liste le contenu du repertoire specifie en argument

# Verification du nombre d'arguments
if [ $# -ne 1 ]
then
   echo "Erreur: usage: $0 <repertoire>"
fi

# Affichage des fichiers
echo "--------------   Fichiers dans $1 --------------------"
for nom in $1/*
do
 if [ -f $nom ]
 then
    echo `basename $nom`
 fi
done

# Affichage des repertoires
echo "--------------   Repertoires dans $1 --------------------"
for nom in $1/*
do
 if [ -d $nom ]
 then
    echo `basename $nom`
 fi
done



