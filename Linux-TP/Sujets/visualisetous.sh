#!/bin/bash

# TP 03, Exercice 4


# Verification du nombre d'arguments
if [ $# -ne 1 ]
then
   echo "Erreur: usage: $0 <repertoire>"
fi

for fichier in $1/*
do
   echo "Voulez vous visualiser $fichier ?"
   read reponse
   if [ "$reponse" != "n" ]
   then
      more $fichier
   fi
done
