#!/bin/sh

REV=$( hg log -l1 --template '{rev}:{node|short}\n' )
DATE=$( hg log -l1 --template '{date|isodate}\n' | cut -c1-10 )

# REV=$(svn st -v linux-memento.tex p*.tex |cut -b 25- |sort -n -r |head -n1 |cut -f1 -d' ')

echo "\date{ Version \docVersion \qquad r. $REV \qquad $DATE }" > autodate.tex

pdflatex linux-memento.tex
