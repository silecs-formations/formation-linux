\select@language {french}
\contentsline {section}{\numberline {1}Commandes et fichiers utilisateurs}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Documentation}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Information sur les ex\IeC {\'e}cutables}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Manipulation des fichiers et r\IeC {\'e}pertoires}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Filtres textes}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}\IeC {\'E}diteurs de texte et utilitaires}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Contr\IeC {\^o}le des processus et des ressources}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Impression et pr\IeC {\'e}-visualisation}{4}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Connexions, sessions, SSH}{4}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Compilation d'un logiciel}{4}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}X11}{4}{subsection.1.10}
\contentsline {section}{\numberline {2}Commandes et fichiers administrateur}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Gestion des utilisateurs}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}D\IeC {\'e}marrage, \textit {runlevels}, services, planification}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Logs}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Gestion des syst\IeC {\`e}mes de fichiers}{6}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Gestion des p\IeC {\'e}riph\IeC {\'e}riques blocs et pseudo-p\IeC {\'e}riph\IeC {\'e}riques}{6}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Supervision des ressources}{7}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Configuration du mat\IeC {\'e}riel}{7}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Archives et sauvegardes}{7}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}R\IeC {\'e}seau}{8}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Gestion des paquets Debian}{8}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Gestion des paquets RPM}{8}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Noyau et modules}{9}{subsection.2.12}
\contentsline {section}{\numberline {3}R\IeC {\'e}f\IeC {\'e}rence Bash}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Fichiers utilis\IeC {\'e}s par bash}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Tubes et redirections}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}D\IeC {\'e}veloppements}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Globbing des noms de fichiers}{10}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Alias et fonctions}{10}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Raccourcis clavier}{11}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Variables courantes}{11}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}D\IeC {\'e}veloppements des variables}{11}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Tests}{12}{subsection.3.9}
\contentsline {section}{\numberline {4}R\IeC {\'e}f\IeC {\'e}rences diverses}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Arborescence Unix standard}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Expressions r\IeC {\'e}guli\IeC {\`e}res}{13}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}L'essentiel de screen}{13}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Kit de survie en vi(m)}{14}{subsection.4.4}
