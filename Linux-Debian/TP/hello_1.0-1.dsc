Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: all
Version: 1.0-1
Maintainer: Guillaume Allegre <Guillaume.Allegre@silecs.info>
Homepage: http://www.sukria.net/fr/libreast/
Standards-Version: 3.8.4
Build-Depends: debhelper (>= 7.0.50~)
Checksums-Sha1: 
 104d8dec05cc6ab08c9f3adde784911e42d6fbab 288 hello_1.0.orig.tar.gz
 9b732708ff75f3e94118906dde56d21afe725b94 1976 hello_1.0-1.debian.tar.gz
Checksums-Sha256: 
 05103af29f0a8f3136f51a6ec301297e3adf26a38d02f928d4d4e5a7d4b52462 288 hello_1.0.orig.tar.gz
 5e928dcf46ca549a5bc91742c80f9e61d8e776bede661d9d6bd9a0cdfeb8d9b3 1976 hello_1.0-1.debian.tar.gz
Files: 
 c8c73252fe9d6fc64287c2f8b35d39de 288 hello_1.0.orig.tar.gz
 d84c2894ed43d4926b5f6a45020057bf 1976 hello_1.0-1.debian.tar.gz
