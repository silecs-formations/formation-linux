Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: all
Version: 1.0-2
Maintainer: Guillaume Allegre <Guillaume.Allegre@silecs.info>
Homepage: http://www.sukria.net/fr/libreast/
Standards-Version: 3.8.4
Build-Depends: debhelper (>= 7.0.50~)
Checksums-Sha1: 
 104d8dec05cc6ab08c9f3adde784911e42d6fbab 288 hello_1.0.orig.tar.gz
 863361a15c31c624b6a71cedfb7482fe5621e8aa 2200 hello_1.0-2.debian.tar.gz
Checksums-Sha256: 
 05103af29f0a8f3136f51a6ec301297e3adf26a38d02f928d4d4e5a7d4b52462 288 hello_1.0.orig.tar.gz
 c3aa8ad068afbebb06f959d597d078dbd6e64f9f3aa2f38fa4417e6db9afdc95 2200 hello_1.0-2.debian.tar.gz
Files: 
 c8c73252fe9d6fc64287c2f8b35d39de 288 hello_1.0.orig.tar.gz
 ba2d978b617de9bace7154ecb8cfb81c 2200 hello_1.0-2.debian.tar.gz
