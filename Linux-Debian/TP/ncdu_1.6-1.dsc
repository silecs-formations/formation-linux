-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: ncdu
Binary: ncdu
Architecture: any
Version: 1.6-1
Maintainer: Eugene V. Lyubimkin <jackyf@debian.org>
Homepage: http://dev.yorhel.nl/ncdu/
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 7), autotools-dev, libncurses5-dev, libncursesw5-dev
Checksums-Sha1: 
 8de9954c119045ed01e447aac9a5def4de1a56fb 104265 ncdu_1.6.orig.tar.gz
 3ad123cf4dd089dcb9730d69c2aa19c5866d53df 2089 ncdu_1.6-1.diff.gz
Checksums-Sha256: 
 0922916eab6371adb2f7a567bf4477c0a738910e799dbdf477f30d138eff470c 104265 ncdu_1.6.orig.tar.gz
 3e58f7d9a5b787e0cd5160ae81e9705d33899bf6efe281c547bcdc3112161bbb 2089 ncdu_1.6-1.diff.gz
Files: 
 95d29cf64af2d8cf4b5005e6e3d60384 104265 ncdu_1.6.orig.tar.gz
 cc63718815cc512206d00c965147e9c9 2089 ncdu_1.6-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAkrleZEACgkQchorMMFUmYxykgCfT5P/rhgz3ddXwLD1NUczIG7J
9P8AoLa/Q/xn6QNROO5FQHaZA/n9JYnq
=4V5U
-----END PGP SIGNATURE-----
