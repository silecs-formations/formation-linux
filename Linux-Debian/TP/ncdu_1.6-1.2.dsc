Format: 1.0
Source: ncdu
Binary: ncdu
Architecture: any
Version: 1.6-1.2
Maintainer: Eugene V. Lyubimkin <jackyf@debian.org>
Homepage: http://dev.yorhel.nl/ncdu/
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 7), autotools-dev, libncurses5-dev, libncursesw5-dev
Checksums-Sha1: 
 8de9954c119045ed01e447aac9a5def4de1a56fb 104265 ncdu_1.6.orig.tar.gz
 767e261be977415e852acc1c0442067ba7c3c285 2213 ncdu_1.6-1.2.diff.gz
Checksums-Sha256: 
 0922916eab6371adb2f7a567bf4477c0a738910e799dbdf477f30d138eff470c 104265 ncdu_1.6.orig.tar.gz
 873aea9305014011819dcc018515957cac4b905dc2c947ffa80c0f3e3e5f9988 2213 ncdu_1.6-1.2.diff.gz
Files: 
 95d29cf64af2d8cf4b5005e6e3d60384 104265 ncdu_1.6.orig.tar.gz
 38b4f0eea1d50409ef88b84d3973ebb2 2213 ncdu_1.6-1.2.diff.gz
