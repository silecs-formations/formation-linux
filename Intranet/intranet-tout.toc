\beamer@endinputifotherversion {3.33pt}
\select@language {french}
\beamer@sectionintoc {1}{Intranet}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Recul}{3}{0}{1}
\beamer@sectionintoc {2}{Infrastructure}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{DHCP}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{DNS}{10}{0}{2}
\beamer@subsectionintoc {2}{3}{NTP}{18}{0}{2}
\beamer@sectionintoc {3}{NFS et r\IeC {\'e}seaux Unix}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{NFS}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{NIS}{29}{0}{3}
\beamer@subsectionintoc {3}{3}{Impression}{34}{0}{3}
\beamer@sectionintoc {4}{Samba}{38}{0}{4}
\beamer@subsectionintoc {4}{1}{Samba}{38}{0}{4}
\beamer@subsectionintoc {4}{2}{NetBIOS}{43}{0}{4}
\beamer@subsectionintoc {4}{3}{SMB / CIFS}{48}{0}{4}
\beamer@sectionintoc {5}{Sauvegarde et archivage}{71}{0}{5}
\beamer@sectionintoc {6}{Supervision r\IeC {\'e}seau}{80}{0}{6}
\beamer@subsectionintoc {6}{1}{Intro}{81}{0}{6}
\beamer@subsectionintoc {6}{2}{Pratique}{82}{0}{6}
\beamer@subsectionintoc {6}{3}{SNMP}{85}{0}{6}
