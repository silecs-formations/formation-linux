Ce répertoire héberge les modules "intranet sous Linux".
Ils sont prévus pour 0,5 à 1 jour chacun.
Les versions approfondies (LAMP...) sont placées dans des répertoires spécifiques.


Liste non exhaustive :
- infra : DNS (Bind9) + DHCP + NTP
- NIS + NFS
- Mail : Postfix + IMAP
- LAMP : initiation
- Samba : initiation
- OpenLDAP
- SVN + FSVS 
- Sauvegardes : BackupPC

