Intranet : panorama des solutions libres (sous Linux)

Objectifs :
Cette formation a pour but de présenter un large panorama des 
différents services et technologies intranet que l'on peut trouver sur un 
serveur Linux, tous basés sur des logiciels libres.
Chaque module est prévu pour une demi-journée, avec un objectif de prise
en main : installation et configuration de base.
Ce tour d'horizon pourra éventuellement permettre aux stagiaires de choisir
une ou plusieurs formations détaillées complémentaires, 
chacune centrée sur l'un des thèmes précis.


Public concerné et pré-requis :
Une bonne connaissance de l'administration générale d'un serveur Linux
est nécessaire.
La formation se fera sur une distribution Debian GNU/Linux et utilisera 
les conventions de cette distribution.

Durée : 4 jours


1. Infrastructure 
 - Serveur DHCP 
 - Serveur DNS (Bind9) : cache et serveur de zone
 - Serveur NTP : synchronisation des machines d'un réseau

2. Serveur de mail
 - Postfix : le serveur SMTP 
 - IMAP : le serveur de réception

3. Partages Unix 
 - NFS : partage de fichiers
 - NIS : annuaire partagé des utilisateurs
 - CUPS : serveur d'impression Unix

4. LDAP : l'annuaire multi-plateformes
 - OpenLDAP : la solution libre
 - les interfaces graphiques et web
 - intégration Unix (pam_ldap) et Windows (samba+ldap)


5. Samba  : cohabitation Unix - MS-Windows
 - partage de fichiers
 - partage d'imprimantes
 - serveur d'authentification

6. Sauvegardes et archivage
 - rsync : commande et service
 - L'application BackupPC

7. Serveur LAMP
 - Apache : le serveur web
 - PHP : le langage web
 - MySQL : serveur de bases de données

8. WebDAV + SVN (OPTIONNEL)
 - WebDav
 - SVN comme serveur de fichiers

9. Supervision réseau
 - SNMP : le protocole de supervision réseau
 - Nagios, logiciel web de supervision

