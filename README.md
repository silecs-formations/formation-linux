Ce dépôt contient les formations effectuées par [Silecs](http://www.silecs.info)
concernant Linux, MySQL, Apache, et d'autres logiciels d'infrastructure libres.

La plupart des supports sont rédigés en 
[LaTeX/beamer](https://bitbucket.org/rivanvx/beamer/wiki/Home).

Tous les supports sont placés sous licence Creative Commons 
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) 
(cf. LICENSE.md) et à ce titre réutilisables et rediffusables sous cette licence.

J'apprécie tout retour sur les supports fournis, 
correction, suggestion, etc. ainsi que
tout retour d'expérience sur leur emploi.

